import { combineReducers } from "redux";
import { btFormReducer } from "./btFrom/slice";

export const rootReducer = combineReducers({
  btForm: btFormReducer,
});
