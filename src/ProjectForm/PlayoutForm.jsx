import React from "react";
import ListProductForm from "./ListProductForm";
import FormTable from "./FormTable";
import "./style.scss";
const PlayoutForm = () => {
  return (
    <div className="container PlayoutForm ">
      <ListProductForm />
      <FormTable />
    </div>
  );
};

export default PlayoutForm;
