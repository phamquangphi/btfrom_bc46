import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormAction } from "../store/btFrom/slice";

const FormTable = () => {
  const dispatch = useDispatch();
  const { productList } = useSelector((state) => state.btForm);
  const [searchTerm, setSearchTerm] = useState(""); // từ khóa tìm kiếm
  const [searchResults, setSearchResults] = useState([]); // kết quả tìm kiếm
  //xử lý sự kiện thay đổi giá trị trong ô input search
  const handleChange = (event) => {
    setSearchTerm(event.target.value);
  };
  //xử lý sự kiện khi nhấn nút search
  const handlSearch = () => {
    const result = productList.filter((pr) =>
      pr.name.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setSearchResults(result);
  };
  //danh sách được hiển thị kết quả hoặc danh sách tìm kiếm ban đầu
  const productsToDisplay = searchTerm ? searchResults : productList;
  return (
    <div className="mt-4">
      <div className=" form-group row">
        <div className="col-10">
          <input
            class="form-control "
            type="search"
            placeholder="Search..."
            value={searchTerm}
            onChange={handleChange}
          />
        </div>
        <div className="col-2">
          <button class="btn btn-outline-success  " onClick={handlSearch}>
            Search
          </button>
        </div>
      </div>
      <table className="table mt-3 ">
        <thead className="bg-dark text-warning">
          <tr>
            <th>Mã SV</th>
            <th>Họ Tên</th>
            <th>Số điện thoại</th>
            <th>Email</th>
            <th>Chức năng</th>
          </tr>
        </thead>
        <tbody>
          {productsToDisplay?.map((pr) => (
            <tr key={pr?.id}>
              <td>{pr?.id}</td>
              <td>{pr?.name}</td>
              <td>{pr?.number}</td>
              <td>{pr?.email}</td>
              <td>
                <button
                  className="btn btn-primary"
                  onClick={() => {
                    dispatch(btFormAction.editProduct(pr));
                  }}
                >
                  <i class="fa-solid fa-user-pen"></i>
                </button>
                <button
                  className="btn btn-danger ml-2"
                  onClick={() => {
                    dispatch(btFormAction.deletProduct(pr.id));
                  }}
                >
                  <i class="fa-solid fa-trash-can"></i>
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default FormTable;
