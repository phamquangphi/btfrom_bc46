import React, { useEffect, useState } from "react";
import "./style.scss";
import { useDispatch, useSelector } from "react-redux";
import { btFormAction } from "../store/btFrom/slice";

const ListProductForm = () => {
  const { productList } = useSelector((state) => state.btForm);
  const { productEdit } = useSelector((state) => state.btForm);
  console.log(productList);
  const [formData, setFormData] = useState();
  const [formError, setFormError] = useState();

  const dispatch = useDispatch();

  //curring function
  const hanhdleFormData = () => (event) => {
    const { name, value, minLength, max, title, validity } = event.target;
    let messager;
    if (minLength !== -1 && !value.length) {
      messager = `Vui lòng nhập thông tin ${title}`;
    } else if (value.length < minLength || (value.length > max && max)) {
      messager = `Vui lòng nhập tối thiểu ${minLength} tới ${max} kí tự`;
    } else if (validity.patternMismatch && ["id", "number"].includes(name)) {
      messager = `Vui lòng nhập kí tự là số`;
    } else if (name === "id") {
      const ID = productList?.find((check) => check.id === value);
      if (ID) messager = `Mã sinh viên tồn tại `;
    } else if (validity.patternMismatch && ["email"].includes(name)) {
      messager = `Vui lòng nhập đúng email`;
    }

    setFormError({
      ...formError,
      [name]: messager,
    });
    setFormData({
      ...formData,
      [name]: messager ? undefined : value,
    });
  };

  // edit feature
  useEffect(() => {
    if (!productEdit) return;
    setFormData(productEdit);
  }, [productEdit]);

  return (
    <div className="ListProductForm">
      <form
        onSubmit={(event) => {
          event.preventDefault();
          // check validation in bottun button when onsubmit
          const check = document.querySelectorAll("input");
          let formError = {};
          check.forEach((vali) => {
            const { name, value, minLength, max, title, validity } = vali;
            let messager;
            if (minLength !== -1 && !value.length) {
              messager = `Vui lòng nhập thông tin ${title}`;
            } else if (
              value.length < minLength ||
              (value.length > max && max)
            ) {
              messager = `Vui lòng nhập tối thiểu ${minLength} tới ${max} kí tự`;
            } else if (
              validity.patternMismatch &&
              ["id", "number"].includes(name)
            ) {
              messager = `Vui lòng nhập kí tự là số`;
            } else if (name === "id" && !productEdit) {
              const ID = productList?.find((check) => check.id === value);
              if (ID) messager = `Mã sinh viên tồn tại `;
            } else if (validity.patternMismatch && ["email"].includes(name)) {
              messager = `Vui lòng nhập đúng email`;
            }
            formError[name] = messager;
          });
          //browse the form's condition
          let list = false;
          for (let key in formError) {
            if (formError[key]) {
              list = true;
              break;
            }
          }
          if (list) {
            setFormError(formError);
            return;
          }

          if (productEdit) {
            dispatch(btFormAction.updateProduct(formData));
          } else {
            dispatch(btFormAction.addProduct(formData));
          }
        }}
        noValidate
      >
        <h2 className="px-2 py-4 bg-dark text-warning">Thông Tin Sinh Viên</h2>
        <div className="form-group row">
          <div className="col-6">
            <p>Mã SV</p>
            <input
              value={formData?.id}
              disabled={!!productEdit}
              type="text"
              className="form-control"
              placeholder="Nhập mã sinh viên"
              name="id"
              title="id"
              minLength={3}
              max={6}
              pattern="^[0-9]+$"
              onChange={hanhdleFormData()}
            />

            <span className="text-danger">{formError?.id}</span>
          </div>
          <div className="col-6">
            <p>Họ tên</p>
            <input
              value={formData?.name}
              type="text"
              className="form-control"
              placeholder="Nhập Họ và Tên"
              name="name"
              title="Họ và Tên"
              minLength={1}
              onChange={hanhdleFormData()}
            />
            <span className="text-danger">{formError?.name}</span>
          </div>
        </div>
        <div className="form-group row">
          <div className="col-6">
            <p>Số điện thoại</p>
            <input
              value={formData?.number}
              type="text"
              className="form-control"
              placeholder="Nhập số điện thoại"
              name="number"
              title="Số điện thoại"
              minLength={1}
              pattern="^[0-9]+$"
              onChange={hanhdleFormData()}
            />
            <span className="text-danger">{formError?.number}</span>
          </div>
          <div className="col-6">
            <p>Email</p>
            <input
              value={formData?.email}
              type="text"
              className="form-control"
              placeholder=".....@gmail.com"
              name="email"
              title="email"
              minLength={1}
              pattern="[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$"
              onChange={hanhdleFormData()}
            />
            <span className="text-danger">{formError?.email}</span>
          </div>
        </div>
        {!productEdit && (
          <button className="btn btn-success">Thêm Sinh Viên</button>
        )}
        {productEdit && (
          <button className="btn btn-warning">Update sinh viên</button>
        )}
      </form>
    </div>
  );
};

export default ListProductForm;
