import logo from "./logo.svg";
import "./App.css";
import PlayoutForm from "./ProjectForm/PlayoutForm";

function App() {
  return (
    <div>
      <PlayoutForm />
    </div>
  );
}

export default App;
