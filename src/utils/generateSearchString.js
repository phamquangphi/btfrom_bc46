import qs from "qs";
export const generateSearchString = (param) => {
  const searchString = qs.stringify(param, {
    addQueryPrefix: true,
  });
  return searchString;
};
